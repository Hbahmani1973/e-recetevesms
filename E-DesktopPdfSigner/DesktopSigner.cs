﻿extern alias ITextSharp;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Deployment.Application;
using DesktopPdfSigner.DTO.PDFSignDTO;
using System.Windows.Forms;
using System.Web;
using System.IO;
using ITextSharp::iTextSharp.text.pdf;
using System.Collections.Generic;
using ITextSharp::iTextSharp.text.pdf.security;
using DesktopPdfSigner.utils;
using tr.gov.tubitak.uekae.esya.api.xmlsignature;
using tr.gov.tubitak.uekae.esya.api.xmlsignature.config;
using tr.gov.tubitak.uekae.esya.api.smartcard.pkcs11;
using tr.gov.tubitak.uekae.esya.api.asn.x509;
using tr.gov.tubitak.uekae.esya.api.common.crypto;
using tr.gov.tubitak.uekae.esya.api.smartcard.util;
using tr.gov.tubitak.uekae.esya.api.crypto.alg;
using Constants = DesktopPdfSigner.utils.Constants;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Linq;
using RISE.Service.WEB._TOOL;
using PayFlex.Smartbox.EDesktopPdfSigner;
using PayFlex.Smartbox.EDesktopPdfSigner.Properties;
using tr.gov.tubitak.uekae.esya.api.common.util;
using DesktopPdfSigner.SmartCard;
using PayFlex.Smartbox.EDesktopPdfSigner.ReceteWs;
using System.Xml.Serialization;
using PayFlex.Smartbox.EDesktopPdfSigner.MedulaWS;

namespace DesktopPdfSigner
{

    public partial class Form1 : Form
    {
        private PdfRequestDTO requestDTO;
        public static string user;
        public static string pass;
        public static string WsPdfAddress;
        public static string WsXsigAddress;
        private string wssend = "";
        public static string valuecell;
        public static int sayi = 0;
        public static long raporkodu = 0;
        public static string[] filefile = new string[1000];
        public static DataTable dt = new DataTable();
        public static BindingSource bs = new BindingSource();
        public static DataTable dt1 = new DataTable();
        public static BindingSource bs1 = new BindingSource();

        string line = "";
        long[] id = new long[1000];
        bool checkedall = false;
        long[] RaporKodu = new long[1000];
        string[] adi = new string[1000];
        string[] Fullpath = new string[1000];
        string[] LinkPath = new string[1000];
        RISEReportData[] reports = new RISEReportData[1000];
        CheckBox headerCheckBox = new CheckBox();
        public Form1()
        {
            InitializeComponent();
            requestDTO = new PdfRequestDTO();
            bckWorker.DoWork += bckWorker_doWork;
            bckWorker.RunWorkerCompleted += bckWorker_RunWorkerCompleted;
        }
   
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                if (Program.SilImza == "S")
                    btnSign.Text = "Imzala";
                else
                    btnSign.Text = "Sil";

                string temp = Application.StartupPath;
                this.requestDTO.pdfContent = new byte[1];
                this.requestDTO.pdfContent = File.ReadAllBytes(temp+"\\Ayar.pdf");
                LicenseUtil.setLicenseXml(new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "lisans.xml"), FileMode.Open, FileAccess.Read));
                SmartCardManager smartCardManager = SmartCardManager.getInstance();
                var smartCardCertificate = smartCardManager.getSignatureCertificate(true, false);
                DateTime? sertifikatarih = smartCardCertificate.getNotAfter();
                DateTime chek = Convert.ToDateTime(sertifikatarih);
                if(chek < new DateTime())
                {
                    WebsocketServer.Sender("Sertifika süreniz bitmiş lütfen kontrol edin");
                    Application.Exit();
                }
                txtSahib.Text = Convert.ToString(smartCardCertificate.getSubject().getCommonNameAttribute());
                
                txtTC.Text = Convert.ToString(smartCardCertificate.getSubject().getSerialNumberAttribute());

                // Process line

                bw3.RunWorkerAsync();
                //this.Activated += AfterLoading;

            }
            //catch(Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    Application.Exit();
            //}
            catch (Exception ex)
            {
                if(Program.CardMessage!="")
                    WebsocketServer.Sender(Program.CardMessage);
                else

                WebsocketServer.Sender(ex.Message);              

            }
            //InitalizeQuery();
       
        }
        /// <summary>
        /// ClickOnce uygulamasını publish ederseniz queryString ile çağırdğınız da aşağıdaki methodu çağırmalısınız.
        /// </summary>
        private void InitalizeQuery()
        {
            string queryString = "";

            NameValueCollection nameValueTable = new NameValueCollection();
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                queryString = ApplicationDeployment.CurrentDeployment.ActivationUri.Query;
                nameValueTable = HttpUtility.ParseQueryString(queryString);
            }
        }
        public String AkilliKartlaImzala(string dosyaYolu)
        {
            string retSignedXmlPath = null;

                Constants.SING_DOCUMENT = dosyaYolu;



                LisansKontrol.LoadFreeLicense();
                try
                {
                    string currentDirectory11 = Directory.GetCurrentDirectory();
                    var currentDirectory = currentDirectory11 + "\\xmlsignature-config.xml";

                    var context = new Context(currentDirectory) { ValidateCertificates = true };

                    context.Config = new Config(currentDirectory);

                    var signature = new XMLSignature(context) { SigningTime = DateTime.Now };

                    //Transforms tt = new Transforms(context);
                    //Transform tr = new Transform(context, SignatureType.XAdES_BES.ToString());
                    //tt.addTransform(tr);
                    signature.addDocument(dosyaYolu, null, true);
                    var terminals = SmartOp.getCardTerminals();
                    if (terminals != null && terminals.Length > 0)
                    {
                        var cardTypeAndSlot = SmartOp.getSlotAndCardType(terminals[0].ToString());
                        var slot = cardTypeAndSlot.getmObj1();
                        var cardType = cardTypeAndSlot.getmObj2();
                        var smartCard = new tr.gov.tubitak.uekae.esya.api.smartcard.pkcs11.SmartCard(cardType);
                        var session = smartCard.openSession(slot);
                        //smartCard.login(session, Constants.SMART_CARD_PIN);

                        var signatureCertificates = smartCard.getSignatureCertificates(session);
                        if (signatureCertificates == null || signatureCertificates.Count == 0)
                        {
                        WebsocketServer.Sender("Kartın içerisinde sertifika bulunamadı.");
                            return null;
                        }
                        ECertificate signingCert = null;
                        foreach (byte[] certByte in signatureCertificates)
                        {
                            var tmpCert = new ECertificate(certByte);
                            if (tmpCert.isQualifiedCertificate())
                            {
                                signingCert = tmpCert;
                                break;
                            }
                        }
                        if (signingCert == null)
                        {
                        WebsocketServer.Sender("Kartın içerisinde sertifika bulunamadı.");
                            return null;
                        }

                        BaseSigner signer = new SCSignerWithCertSerialNo(smartCard, session, slot, signingCert.getSerialNumber().GetData(), SignatureAlg.RSA_SHA256.getName());
                        //if (Constants.SMART_CARD_TC != Constants.SMART_CARD_TC_GECERLI)
                        //{
                        //    var validCertificate = SertifikaKontrol(signingCert);
                        //    if (validCertificate)
                        //    {
                        //        //MessageBox.Show("İmza atılmak istenen sertifika geçerli değil.");
                        //        return null;
                        //    }
                        //}

                        signature.addKeyInfo(signingCert);
                        signature.sign(signer);
                        var sourceFileInfo = new FileInfo(dosyaYolu);
                        if (sourceFileInfo.Directory != null)
                        {
                            var destDirPath = sourceFileInfo.Directory.FullName;
                            retSignedXmlPath = destDirPath + @"\" + sourceFileInfo.Name.Replace(".xml", ".xsig");
                        }
                        var signatureFileStream = new FileStream(retSignedXmlPath, FileMode.Create);
                        signature.write(signatureFileStream);
                        signatureFileStream.Close();
                        //  MessageBox.Show("Elektronik imza işlemi tamamlandı", "Elektronik imza", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }
                    else
                    {
                    WebsocketServer.Sender("Kart okunamadı.");
                        return null;
                    }


                }
                catch (XMLSignatureException exc)
                {
                WebsocketServer.Sender("Hata Oluştu." + exc.Message);
                Thread.Sleep(2000);
                    Application.Exit();
                }
                catch (Exception exc)
                {
                WebsocketServer.Sender("Hata Oluştu." + exc.Message);
                Thread.Sleep(2000);
                Application.Exit();
                }

            
            return retSignedXmlPath; 


        }
        public void LoadingCircleStart(MRG.Controls.UI.LoadingCircle loCircle, int PrmNspoke, int PrmThickness, int PrmInnerRadius, int PrmOuterRadius, int PrmRotSpeed)
        {
            loCircle.Invoke((MethodInvoker)(() => loCircle.Visible = true));
            loCircle.StylePreset = MRG.Controls.UI.LoadingCircle.StylePresets.Firefox;
            loCircle.NumberSpoke = PrmNspoke;
            loCircle.SpokeThickness = PrmThickness;
            loCircle.InnerCircleRadius = PrmInnerRadius;
            loCircle.OuterCircleRadius = PrmOuterRadius;
            loCircle.RotationSpeed = PrmRotSpeed;
            loCircle.Color = System.Drawing.Color.Blue;
            loCircle.Invoke((MethodInvoker)(() => loCircle.BringToFront()));
            loCircle.Invoke((MethodInvoker)(() => loCircle.Active = true));
        }
        private void btnSign_Click(object sender, EventArgs e)
        {
            loadingCircle.Visible = true;
            LoadingCircleStart(loadingCircle, 12, 6, 5, 53, 100);
            if (!string.IsNullOrEmpty(txtUsbDonglePassword.Text))
            {
                btnSign.Text = "İmzalanıyor , lütfen bekleyiniz.";
                btnSign.Enabled = false;
                this.requestDTO.DonglePassword = txtUsbDonglePassword.Text;
                bckWorker.RunWorkerAsync();
            }
            else
            {
                WebsocketServer.Sender("USB-Dongle şifrenizi giriniz.");
                LoadingCircleStop(loadingCircle);
                txtUsbDonglePassword.Focus();
            }
        }
        private void bckWorker_doWork(object sender, DoWorkEventArgs e)
        {
            SignatureManager.SignatureManager signManager = new SignatureManager.SignatureManager();
            signManager.SignPdf(this.requestDTO);
        }
        private void bckWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (e.Error != null)
            {
                WebsocketServer.Sender("Hata oluştu : " + e.Error.ToString());
            }
            else if (e.Cancelled == true)
            {
                WebsocketServer.Sender("Operasyon iptal edildi!");
            }
            else
            {
                // MessageBox.Show("Pdf başarıyla imzalandı ve kaydedildi.", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bckWorkerXsig.RunWorkerAsync();
            }
        }

        private void chBoxPassword_CheckedChanged(object sender, EventArgs e)
        {
            txtUsbDonglePassword.PasswordChar = chBoxPassword.Checked ? '\0' : '*';
        }

        private void btnFileUpload_Click(object sender, EventArgs e)
        {
      

        }
        private string checkSignature(byte[] pdfContent)
        {
            PdfReader reader = new PdfReader(pdfContent);

            AcroFields fields = reader.AcroFields;
            List<String> names = fields.GetSignatureNames();

            // Signature eklenmiş PDF dosyası buraya yollanmalı. Yoksa Verification Gerçekleşemez.

            if (names.Count == 0)
            {
                return "İlgili PDF'e ait imza(lar) bulunamamıştır.";
            }
            string message = string.Empty;
            for (int i = 1; i < names.Count + 1; i++)
            {
                string temp = string.Empty;
                PdfPKCS7 pkcs7 = fields.VerifySignature(names[i - 1]);
                var result = pkcs7.Verify();
                if (result)
                {
                    temp = string.Format("{0}.imza geçerli.", i);
                }
                else
                {
                    temp = string.Format("{0}.imza geçersiz.", i);
                }
                message += temp;
            }
            reader.Close();
            return message;
        }
        private void btnCheckSignature_Click(object sender, EventArgs e)
        {
           // var message = checkSignature(this.requestDTO.pdfContent);
         //   MessageBox.Show(message, "Mesaj", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bckWorkerXsig_DoWork(object sender, DoWorkEventArgs e)
        {
            string path = "";
            String str1;
                path = "c:\\Receteler\\" + Program.filename1;
            str1 = AkilliKartlaImzala(path);
        }

        private void bckWorkerXsig_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (Program.SilImza == "S")
                bwImzala.RunWorkerAsync();
            else
                bwSil.RunWorkerAsync();
           
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnImzala_Click(object sender, EventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
  
           
        }

        private void bw3_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadingCircleStart(loadingCircle, 12, 6, 5, 53, 100);
           // Thread.Sleep(1000);
        }

        private void bw3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadingCircleStop(loadingCircle);
            txtUsbDonglePassword.Focus();
         
        }

        private void bw3_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }



     
        public void LoadingCircleStop(MRG.Controls.UI.LoadingCircle loCircle)
        {
            loCircle.Active = false;
            loCircle.Invoke((MethodInvoker)(() => loCircle.Visible = false));
        }

   
      
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
       

        private void bwImzala_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
 
    

        private void bckWorker_DoWork_1(object sender, DoWorkEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
       
        }

        private void loadingCircle_Click(object sender, EventArgs e)
        {

        }

        private void bwImzala_DoWork(object sender, DoWorkEventArgs e)
        {



            try
            {

                var dosya1 = "";


                dosya1 = Program.filename1.Substring(0,Program.filename1.LastIndexOf(".")) + ".xsig";
                byte[] byteRecete = null;
                using (var reader = new StreamReader(Path.Combine("c:\\Receteler\\", dosya1)))
                {
                    byteRecete = Encoding.UTF8.GetBytes(reader.ReadToEnd());
                }
                SgkBaglanti SGK = new SgkBaglanti();
                imzaliEreceteGirisIstekDVO istek = new imzaliEreceteGirisIstekDVO();
                istek.imzaliErecete = byteRecete;
                istek.doktorTcKimlikNo = long.Parse(Program.doktortc);
                istek.surumNumarasi = 1;
                istek.tesisKodu = Program.tesisKodu;
                using (var writer = new StreamWriter(Path.Combine(Program.yol, Program.filename1.Substring(0, Program.filename1.LastIndexOf(".")) + "_istek.xml")))
                {
                    var serialize = new XmlSerializer(typeof(imzaliEreceteGirisIstekDVO));
                    serialize.Serialize(writer, istek);
                    writer.Flush();
                    writer.Close();
                }
                imzaliEreceteGirisCevapDVO cevap = new imzaliEreceteGirisCevapDVO();
                cevap = SGK.imzaliEreceteGonder(Program.tesisKodu.ToString(), Program.MedulaSifre, istek);
                using (var cevapwriter = new StreamWriter(Path.Combine(Program.yol, Program.filename1.Substring(0, Program.filename1.LastIndexOf(".")) + "_MedulaCevap.xml")))
                {
                    var cevapserialize = new XmlSerializer(typeof(imzaliEreceteGirisCevapDVO));
                    cevapserialize.Serialize(cevapwriter, cevap);
                    cevapwriter.Flush();
                    cevapwriter.Close();
                }
                if (object.ReferenceEquals(cevap.sonucKodu, "0000") | object.ReferenceEquals(cevap.sonucKodu, "0") | cevap.sonucMesaji == "")
                {
                    //TopMostMessageBox.Show("Takip Numarasi :" + cevap.ereceteDVO.ereceteNo);
                    WebsocketServer.Sender(cevap.ereceteDVO.ereceteNo);
                    Program.takipno = cevap.ereceteDVO.ereceteNo;
                }
                else
                {
                   // TopMostMessageBox.Show(cevap.sonucMesaji);
                    WebsocketServer.Sender(cevap.sonucMesaji);
                }

            }
            catch (Exception ex)
            {
                WebsocketServer.Sender(ex.Message);
                Thread.Sleep(2000);
                Application.Exit();

            }

        }

        private void bwImzala_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Application.Exit();
        }

        private void bwSil_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (Program.Value ==  "")
                {
                    WebsocketServer.Sender("EREÇETE Numarasi Böş Gönderilemez");
                    Thread.Sleep(1000);
                    Application.Exit();
                }
                var dosya1 = "";


                dosya1 = Program.filename1.Substring(0, Program.filename1.LastIndexOf(".")) + ".xsig";
                byte[] byteRecete = null;
                using (var reader = new StreamReader(Path.Combine("c:\\Receteler\\", dosya1)))
                {
                    byteRecete = Encoding.UTF8.GetBytes(reader.ReadToEnd());
                }
                SgkBaglanti SGK = new SgkBaglanti();
                imzaliEreceteSilIstekDVO istek = new imzaliEreceteSilIstekDVO();
                istek.imzaliEreceteSil = byteRecete;
                istek.doktorTcKimlikNo = Program.doktortc.ToString();
                istek.surumNumarasi = "1";
                istek.tesisKodu = Program.tesisKodu.ToString();
                using (var writer = new StreamWriter(Path.Combine(Program.yol, Program.filename1.Substring(0, Program.filename1.LastIndexOf(".")) + "_istek.xml")))
                {
                    var serialize = new XmlSerializer(typeof(imzaliEreceteSilIstekDVO));
                    serialize.Serialize(writer, istek);
                    writer.Flush();
                    writer.Close();
                }
                imzaliEreceteSilCevapDVO cevap = new imzaliEreceteSilCevapDVO();
                cevap = SGK.imzaliEreceteSil(Program.tesisKodu.ToString(), Program.MedulaSifre, istek);
                using (var cevapwriter = new StreamWriter(Path.Combine(Program.yol, Program.filename1.Substring(0, Program.filename1.LastIndexOf(".")) + "_MedulaCevap.xml")))
                {
                    var cevapserialize = new XmlSerializer(typeof(imzaliEreceteSilCevapDVO));
                    cevapserialize.Serialize(cevapwriter, cevap);
                    cevapwriter.Flush();
                    cevapwriter.Close();
                }
                if (object.ReferenceEquals(cevap.sonucKodu, "0000") | cevap.sonucMesaji == "")
                {
                    //TopMostMessageBox.Show("Reçete Sil Başarili");
                    WebsocketServer.Sender("IsDeleted");
                }
                else
                {
                    //TopMostMessageBox.Show(cevap.sonucMesaji);
                    WebsocketServer.Sender(cevap.sonucMesaji);
                }
            }
            catch (Exception ex)
            {
                WebsocketServer.Sender(ex.Message);
                Application.Exit();

            }
        }

        private void bwSil_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Application.Exit();
        }

        private void bcworkerMessage_DoWork(object sender, DoWorkEventArgs e)
        {
            WebsocketServer.Sender(Program.CardMessage);

        }

        private void bcworkerMessage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Application.Exit();
        }
    }
}
