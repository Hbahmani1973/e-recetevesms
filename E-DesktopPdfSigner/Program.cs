﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Windows.Forms;
using System.Web;
using System.Collections.Specialized;
using System.Threading;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;

namespace DesktopPdfSigner
{
    static class Program
    {
        public static string filename = "";
        public static string filename1 = "";
        public static string[] raporId;
        public static string username = "";
        public static string[] versiyonCheck = null;
        public static string password = "";
        public static string token = "";
        public static string LinkAddress = "";
        public static string LinkPdfAddress = "";
        public static string GetirAddress = "";
        public static string fileDownload = "";
        public static bool input =true;
        public static string doktortc = "";
        public static int tesisKodu = 0;
        public static string yol ="C:\\Receteler";
        public static string SGKTesisKodu = "";
        public static string MedulaSifre = "";
        public static string takipno = "";
        public static string SilImza = "";
        public static string Value = "";
        public static string CardMessage = "";


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                args[0] = args[0].Substring(15);
                args[0] = args[0].Substring(0, args[0].Length - 1);
                //                Program.fileDownload = "http://raporservis.proteksaglik.com:8282/_Uploads/";
                //WebClient web = new WebClient();

                //web.DownloadFile(Program.fileDownload + "VersiyonKontrolu.txt", @"C:\Eimza\VersiyonKontrolu.txt");
                //versiyonCheck = File.ReadAllLines("C:\\Eimza\\VersiyonKontrolu.txt"); 


                //web.DownloadFile(Program.fileDownload + file, @"C:\Eimza\" + file);

                //string a = File.ReadAllText(@"C:\Eimza\" + file);

                WebsocketServer.Aaa();

                byte[] data = Convert.FromBase64String(args[0]);

                string decodedString = Encoding.UTF8.GetString(data);
                var List = decodedString.Split(';');
                //  decodedString = decodedString.Split("");
                Program.filename = List[0];
                Program.filename1 = Program.filename.Substring(Program.filename.LastIndexOf("/") + 1);
                //   File.Copy(Program.filename, @"c:\Receteler\"+Program.filename1);
                WebClient web = new WebClient();

                web.DownloadFile(Program.filename, @"C:\Receteler\"+filename1);
                tesisKodu = Convert.ToInt32(List[1]);
                doktortc = List[2];
                //username = "DR027";
                //password = "123456!";
                MedulaSifre = List[3];
                SilImza = List[4];
                //token = "";
                if (SilImza == "O")
                {
                    XmlDocument _LocalInfo_Xml = new XmlDocument();
                    _LocalInfo_Xml.Load(Program.filename);
                    XmlElement _XmlElement;
                    _XmlElement = _LocalInfo_Xml.GetElementsByTagName("ereceteNo")[0] as XmlElement;
                     Value = _XmlElement.InnerText;
                   
                }
                //  raporId[0] = args[0].Replace("eimza://", "");
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
                //bool openProg;
                //Mutex mtx = new Mutex(true, "Form1", out openProg);

                //if (!openProg)
                //{
                //    MessageBox.Show("Çalıştırmak istediğiniz program zaten açık durumda !!");
                //    return;
                //}
                //else
                //{

                //    
                //}
           
                //GC.KeepAlive(mtx);
            }
            catch(Exception ex)
            {
                WebsocketServer.Sender(ex.Message);
                Application.Exit();
            }
        }
        public static bool IsBase64String(this string s)
        {
            s = s.Trim();
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);

        }



    }
   
}
