﻿namespace DesktopPdfSigner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.txtUsbDonglePassword = new System.Windows.Forms.TextBox();
            this.btnSign = new System.Windows.Forms.Button();
            this.bckWorker = new System.ComponentModel.BackgroundWorker();
            this.chBoxPassword = new System.Windows.Forms.CheckBox();
            this.fileUpload = new System.Windows.Forms.OpenFileDialog();
            this.bckWorkerXsig = new System.ComponentModel.BackgroundWorker();
            this.loadingCircle = new MRG.Controls.UI.LoadingCircle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.btnImzala = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.bw3 = new System.ComponentModel.BackgroundWorker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSahib = new System.Windows.Forms.TextBox();
            this.txtTC = new System.Windows.Forms.TextBox();
            this.bwImzala = new System.ComponentModel.BackgroundWorker();
            this.bwSil = new System.ComponentModel.BackgroundWorker();
            this.bcworkerMessage = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usb-Dongle Şifreniz :";
            // 
            // txtUsbDonglePassword
            // 
            this.txtUsbDonglePassword.Location = new System.Drawing.Point(147, 63);
            this.txtUsbDonglePassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUsbDonglePassword.Name = "txtUsbDonglePassword";
            this.txtUsbDonglePassword.PasswordChar = '*';
            this.txtUsbDonglePassword.Size = new System.Drawing.Size(192, 22);
            this.txtUsbDonglePassword.TabIndex = 1;
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(12, 92);
            this.btnSign.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(352, 42);
            this.btnSign.TabIndex = 2;
            this.btnSign.Text = "İmzala";
            this.btnSign.UseVisualStyleBackColor = true;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // bckWorker
            // 
            this.bckWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bckWorker_DoWork_1);
            // 
            // chBoxPassword
            // 
            this.chBoxPassword.AutoSize = true;
            this.chBoxPassword.Location = new System.Drawing.Point(346, 67);
            this.chBoxPassword.Margin = new System.Windows.Forms.Padding(4);
            this.chBoxPassword.Name = "chBoxPassword";
            this.chBoxPassword.Size = new System.Drawing.Size(18, 17);
            this.chBoxPassword.TabIndex = 3;
            this.chBoxPassword.UseVisualStyleBackColor = true;
            this.chBoxPassword.CheckedChanged += new System.EventHandler(this.chBoxPassword_CheckedChanged);
            // 
            // fileUpload
            // 
            this.fileUpload.FileName = "openFileDialog1";
            // 
            // bckWorkerXsig
            // 
            this.bckWorkerXsig.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bckWorkerXsig_DoWork);
            this.bckWorkerXsig.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bckWorkerXsig_RunWorkerCompleted);
            // 
            // loadingCircle
            // 
            this.loadingCircle.Active = false;
            this.loadingCircle.BackColor = System.Drawing.SystemColors.Control;
            this.loadingCircle.Color = System.Drawing.Color.Transparent;
            this.loadingCircle.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.loadingCircle.InnerCircleRadius = 8;
            this.loadingCircle.Location = new System.Drawing.Point(83, 7);
            this.loadingCircle.Margin = new System.Windows.Forms.Padding(4);
            this.loadingCircle.Name = "loadingCircle";
            this.loadingCircle.NumberSpoke = 24;
            this.loadingCircle.OuterCircleRadius = 9;
            this.loadingCircle.RotationSpeed = 80;
            this.loadingCircle.Size = new System.Drawing.Size(210, 166);
            this.loadingCircle.SpokeThickness = 4;
            this.loadingCircle.StylePreset = MRG.Controls.UI.LoadingCircle.StylePresets.IE7;
            this.loadingCircle.TabIndex = 22;
            this.loadingCircle.Text = "loadingCircle";
            this.loadingCircle.Click += new System.EventHandler(this.loadingCircle_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 329);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(356, 66);
            this.dataGridView1.TabIndex = 23;
            this.dataGridView1.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(422, 352);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnImzala
            // 
            this.btnImzala.BackColor = System.Drawing.Color.Green;
            this.btnImzala.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnImzala.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnImzala.Location = new System.Drawing.Point(422, 277);
            this.btnImzala.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnImzala.Name = "btnImzala";
            this.btnImzala.Size = new System.Drawing.Size(357, 50);
            this.btnImzala.TabIndex = 25;
            this.btnImzala.Text = "Imzala ve Xsig\'e Çevir";
            this.btnImzala.UseVisualStyleBackColor = false;
            this.btnImzala.Click += new System.EventHandler(this.btnImzala_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(19, 291);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(356, 21);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 26;
            this.progressBar1.Visible = false;
            // 
            // bw3
            // 
            this.bw3.WorkerReportsProgress = true;
            this.bw3.WorkerSupportsCancellation = true;
            this.bw3.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw3_DoWork);
            this.bw3.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bw3_ProgressChanged);
            this.bw3.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw3_RunWorkerCompleted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 28;
            this.label2.Text = "Sertifika Sahibi :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 17);
            this.label3.TabIndex = 29;
            this.label3.Text = "TC Kimlik NUmarasi :";
            // 
            // txtSahib
            // 
            this.txtSahib.Location = new System.Drawing.Point(147, 7);
            this.txtSahib.Name = "txtSahib";
            this.txtSahib.Size = new System.Drawing.Size(217, 22);
            this.txtSahib.TabIndex = 30;
            // 
            // txtTC
            // 
            this.txtTC.Location = new System.Drawing.Point(147, 35);
            this.txtTC.Name = "txtTC";
            this.txtTC.Size = new System.Drawing.Size(217, 22);
            this.txtTC.TabIndex = 31;
            // 
            // bwImzala
            // 
            this.bwImzala.WorkerReportsProgress = true;
            this.bwImzala.WorkerSupportsCancellation = true;
            this.bwImzala.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwImzala_DoWork);
            this.bwImzala.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwImzala_RunWorkerCompleted);
            // 
            // bwSil
            // 
            this.bwSil.WorkerReportsProgress = true;
            this.bwSil.WorkerSupportsCancellation = true;
            this.bwSil.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwSil_DoWork);
            this.bwSil.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwSil_RunWorkerCompleted);
            // 
            // bcworkerMessage
            // 
            this.bcworkerMessage.WorkerReportsProgress = true;
            this.bcworkerMessage.WorkerSupportsCancellation = true;
            this.bcworkerMessage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bcworkerMessage_DoWork);
            this.bcworkerMessage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bcworkerMessage_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(374, 186);
            this.Controls.Add(this.txtTC);
            this.Controls.Add(this.txtSahib);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnImzala);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.loadingCircle);
            this.Controls.Add(this.chBoxPassword);
            this.Controls.Add(this.btnSign);
            this.Controls.Add(this.txtUsbDonglePassword);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reçete İmzala ve Sil";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUsbDonglePassword;
        private System.Windows.Forms.Button btnSign;
        private System.ComponentModel.BackgroundWorker bckWorker;
        private System.Windows.Forms.CheckBox chBoxPassword;
        private System.Windows.Forms.OpenFileDialog fileUpload;
        private System.ComponentModel.BackgroundWorker bckWorkerXsig;
        private MRG.Controls.UI.LoadingCircle loadingCircle;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnImzala;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker bw3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSahib;
        private System.Windows.Forms.TextBox txtTC;
        private System.ComponentModel.BackgroundWorker bwImzala;
        private System.ComponentModel.BackgroundWorker bwSil;
        private System.ComponentModel.BackgroundWorker bcworkerMessage;
    }
}

