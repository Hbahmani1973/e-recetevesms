﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows.Forms;

static class TopMostMessageBox
{
    public static DialogResult Show(string message)
    {
        return Show(message, string.Empty, MessageBoxButtons.OK);
    }

    public static DialogResult Show(string message, string title)
    {
        return Show(message, title, MessageBoxButtons.OK);
    }

    public static DialogResult Show(string message, string title, MessageBoxButtons buttons)
    {
        Form topmostForm = new Form();
        topmostForm.Size = new System.Drawing.Size(100, 100);
        topmostForm.StartPosition = FormStartPosition.CenterScreen;

        System.Drawing.Rectangle rect = SystemInformation.VirtualScreen;
        topmostForm.Location = new System.Drawing.Point(rect.Bottom + 10, rect.Right + 10);
        topmostForm.Show();
        topmostForm.Focus();
        topmostForm.BringToFront();
        topmostForm.TopMost = true;
        DialogResult result = MessageBox.Show(topmostForm, message, title, buttons);
        topmostForm.Dispose();
        return result;
    }
}