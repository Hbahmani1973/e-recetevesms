﻿using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Deployment;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using PayFlex.Smartbox.EDesktopPdfSigner.MedulaWS;
using PayFlex.Smartbox.EDesktopPdfSigner.ReceteWs;
using Newtonsoft.Json;
using System;
using System.Text;

namespace PayFlex.Smartbox.EDesktopPdfSigner
{
    public class SgkBaglanti
    {
        public bool ValidateCertificate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public SaglikTesisiRaporIslemleriClient EraporBaglantiKur(string userName, string password)
        {
            try
            {
                CustomBinding binding = new CustomBinding();
                SecurityBindingElement sbe = SecurityBindingElement.CreateUserNameOverTransportBindingElement();
                HttpTransportBindingElement trnsprt = new HttpTransportBindingElement();
                System.ServiceModel.EndpointAddress eps = null/* TODO Change to default(_) if this is not a reference type */;
                sbe.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10;
                sbe.IncludeTimestamp = false;
                sbe.DefaultAlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Basic256;
                binding.Elements.Add(sbe);
                binding.Elements.Add(new TextMessageEncodingBindingElement(MessageVersion.Soap11, System.Text.Encoding.UTF8));
                binding.Elements.Add(new HttpsTransportBindingElement());
                binding.Elements.Find<HttpsTransportBindingElement>().MaxReceivedMessageSize = 2000000000;
                binding.CloseTimeout = TimeSpan.FromSeconds(60000);
                binding.OpenTimeout = TimeSpan.FromSeconds(60000);
                binding.SendTimeout = TimeSpan.FromSeconds(60000);
                binding.ReceiveTimeout = TimeSpan.FromSeconds(60000);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                eps = new System.ServiceModel.EndpointAddress("https://medeczane.sgk.gov.tr/medula/eczane/saglikTesisiRaporIslemleriWS");
                SaglikTesisiRaporIslemleriClient SGKRaporV2 = new SaglikTesisiRaporIslemleriClient(binding, eps);
                SGKRaporV2.ClientCredentials.UserName.UserName = userName;
                SGKRaporV2.ClientCredentials.UserName.Password = password;
                SGKRaporV2.Open();
                return SGKRaporV2;
            }
            catch (Exception ex)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public SaglikTesisiReceteIslemleriClient EreceteBaglantiKur(string userName, string password)
        {
            try
            {
                CustomBinding binding = new CustomBinding();
                SecurityBindingElement sbe = SecurityBindingElement.CreateUserNameOverTransportBindingElement();
                HttpTransportBindingElement trnsprt = new HttpTransportBindingElement();
                System.ServiceModel.EndpointAddress eps = null/* TODO Change to default(_) if this is not a reference type */;
                sbe.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10;
                sbe.IncludeTimestamp = false;
                sbe.DefaultAlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Basic256;
                binding.Elements.Add(sbe);
                binding.Elements.Add(new TextMessageEncodingBindingElement(MessageVersion.Soap11, System.Text.Encoding.UTF8));
                binding.Elements.Add(new HttpsTransportBindingElement());
                binding.Elements.Find<HttpsTransportBindingElement>().MaxReceivedMessageSize = 2000000000;
                binding.CloseTimeout = TimeSpan.FromSeconds(60000);
                binding.OpenTimeout = TimeSpan.FromSeconds(60000);
                binding.SendTimeout = TimeSpan.FromSeconds(60000);
                binding.ReceiveTimeout = TimeSpan.FromSeconds(60000);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                eps = new System.ServiceModel.EndpointAddress("https://medeczane.sgk.gov.tr/medula/eczane/saglikTesisiReceteIslemleriWS");
                SaglikTesisiReceteIslemleriClient SGKReceteV2 = new SaglikTesisiReceteIslemleriClient(binding, eps);
                SGKReceteV2.ClientCredentials.UserName.UserName = userName;
                SGKReceteV2.ClientCredentials.UserName.Password = password;
                SGKReceteV2.Open();
                return SGKReceteV2;
            }
            catch (Exception ex)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public imzaliEraporGirisCevapDVO imzaliEraporGonder(string userName, string password, imzaliEraporGirisIstekDVO imzaliRecete)
        {
            try
            {
                SaglikTesisiRaporIslemleriClient servis = EraporBaglantiKur(userName, password);
                if (servis != null)
                {
                    // '---------------------------------------------------------------------------------------------------
                    imzaliEraporGirisCevapDVO cevap = servis.imzaliEraporGiris(imzaliRecete);
                    return cevap;
                }
                else
                    return null/* TODO Change to default(_) if this is not a reference type */;
            }


            catch (Exception ex)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }
        public imzaliEraporEtkinMaddeEkleCevapDVO imzaliEtkinMaddeGonder(string userName, string Password, imzaliEraporEtkinMaddeEkleIstekDVO imzaliRapor)
        {
            try
            {
                SaglikTesisiRaporIslemleriClient servis = EraporBaglantiKur(userName, Password);
                imzaliEraporEtkinMaddeEkleCevapDVO cevap = servis.imzaliEraporEtkinMaddeEkle(imzaliRapor);
                return cevap;
            }
            catch (Exception ex)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public imzaliEreceteGirisCevapDVO imzaliEreceteGonder(string userName, string password, imzaliEreceteGirisIstekDVO imzaliRecete)
        {
            try
            {
                SaglikTesisiReceteIslemleriClient servis = EreceteBaglantiKur(userName, password);
                if (servis != null)
                {
                    imzaliEreceteGirisCevapDVO cevap = servis.imzaliEreceteGiris(imzaliRecete);
                    return cevap;
                }
                else
                    return null/* TODO Change to default(_) if this is not a reference type */;
            }
            catch (Exception ex)
            {
                DesktopPdfSigner.WebsocketServer.Sender(ex.ToString());
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public imzaliEreceteSilCevapDVO imzaliEreceteSil(string userName, string password, imzaliEreceteSilIstekDVO imzaliRecete)
        {
            try
            {
                SaglikTesisiReceteIslemleriClient servis = EreceteBaglantiKur(userName, password);

                if (servis != null)
                {
                    imzaliEreceteSilCevapDVO cevap = servis.imzaliEreceteSil(imzaliRecete);
                    return cevap;
                }
                else
                    return null/* TODO Change to default(_) if this is not a reference type */;
            }
            catch (Exception ex)
            {
                TopMostMessageBox.Show(ex.ToString());
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public imzaliEraporSilCevapDVO imzaliRaporSil(string userName, string Password, imzaliEraporSilIstekDVO imzaliRapor)
        {
            try
            {
                SaglikTesisiRaporIslemleriClient servis = EraporBaglantiKur(userName, Password);
                imzaliEraporSilCevapDVO cevap = servis.imzaliEraporSil(imzaliRapor);
                return cevap;
            }
            catch (Exception ex)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public imzaliEraporBashekimOnayCevapDVO imzaliBashekimOnay(string userName, string Password, imzaliEraporBashekimOnayIstekDVO imzaliRapor)
        {
            try
            {
                SaglikTesisiRaporIslemleriClient servis = EraporBaglantiKur(userName, Password);
                imzaliEraporBashekimOnayCevapDVO cevap = servis.imzaliEraporBashekimOnay(imzaliRapor);
                return cevap;
            }
            catch (Exception ex)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }
      
    }
}
